# League of Legends TeamBuilder

# Where to View Code

All of our code for the application was done in TeamBuilder.java which is located at LeagueofLegends/src/TeamBuilder.java.

We used the IDE Eclipse to write and run the code, but any IDE should probably work. If there are problems, I can provide

a demo of the code over Zoom.



# Running the Program

When you run the program, it should print out the name of five champions and what position they play. Every position

must be a different champion like how League of Legends works.



# Logic of the Algorithm

I separated all the champions into arrays of what position they could play. There are lists for champions that only play

one position and lists for champions that play more than one. Champions were sorted accordingly. This algorithm is different

than the solution in the research paper in two ways: the order in which champions were chosen and how repeated champions

were addressed. In the paper, our steps began with champion type that we thought would be the easiest to write cases for, such

as bottom and jungle, because those champions mostly could not play other positions. In the code, we chosen champions in an

arbitrary order because it didn't complicate the code like it would have with the combinatorial cases and calculations. We could

change the order they're chosen if we wanted to without having to make many changes in the code. The second difference I mentioned

was how we accounted for repetition. In the research paper, we would have cases where if that champion could play more than one position

such as Top and Bottom. If it was chosen for Top, then we would remove it from the set for Bottom to consider. In the code, however,

we do not remove the champion. We pick a random champion and check if it was already selected, if it was, then we continue randomly

picking one until it is not a duplicate. 