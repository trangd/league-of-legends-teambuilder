import java.util.Random;

public class TeamBuilder {
    /*
     * Placeholders for champions on a team
     */
    private String topChamp = "";
    private String botChamp = "";
    private String midChamp = "";
    private String supChamp = "";
    private String junChamp = "";
    
    /*
     * Lists all champions that only play top, middle, bottom, support and jungle
     */
    private String[] top = {"Aatrox", "Cho", "Darius", "Mundo", "Fiora", "Gangplank",
        "Garen", "Gnar", "Illaoi", "Jayce", "Kayle", "Kennen", "Kled", "Mord", "Nasus",
        "Omn", "Poppy", "Quinn", "Renekton", "Riven", "Singed", "Teemo", "Tryndamere",
        "Yorick", "Heimerdinger"};
    
    private String[] mid = {"Ahri", "Anivia", "Annie", "Aurelion", "Azir", "Cass",
        "Corki", "Diana", "Ekko", "Fizz", "Kassadin", "Katarina", "Leblanc", "Lissandra",
        "Malz", "Neeko", "Orianna", "Qiyana", "Syndra", "Talon", "Twisted Fate", 
        "Vicktor", "Zed", "Ziggs", "Zoe", "Taliyah"};
    
    private String[] bot = {"Aphelios", "Ashe", "Caitlyn", "Draven", "Ezreal", "Jhin",
        "Jinx", "Kaisa", "Kalista", "Kog", "Samira", "Sivir", "Tristana", "Twitch"};
    
    private String[] sup = {"Alistar", "Bard", "Blitz", "Brand", "Braum", "Janna", "Karma", 
        "Leona", "Lulu", "Morgana", "Nami", "Nautilus", "Pantheon", "Pyke", "Rakan", "Sona", 
        "Soraka", "Swain", "Taric", "Thresh", "Yuumi", "Zilean", "Zyra", "Tahm"};
    
    private String[] jungle = {"Amumu", "Elise", "Evelynn", "Fiddles", "Gragas", "Graves", 
        "Hecarim", "Ivern", "Jarvan", "Karthus", "Kennen", "Khazix", "Kindred", "Lee Sin", 
        "Lillia", "Master Yi", "Nidalee", "Nocturne", "Nunu", "Olaf", "Rammus", "Reksai", 
        "Rengar", "Sejuani", "Shaco", "Shyvana", "Skarner", "Udyr", "Vi", "Warwick", 
        "Xin Zhao", "Zac"};
 
    
    private String[] topmid = {"Akali", "Irelia", "Rumble", "Ryze", "Sylas", "Vladimir", "Yone"};
    private String[] topjun = {"Jax", "Sion", "Urgot", "Volibear", "Wukong"};
    private String[] topsup = {"Camille", "Malp", "Maokai", "Sett", "Shen"};
    private String[] midbot = {"Lucian", "Veigar", "Yasuo"};
    private String[] midsup = {"Galio", "Lux", "Velkoz", "Xerath"};
    private String[] botsup = {"Senna", "Master Yi"};
    
    /**
     */
    public TeamBuilder() {
        topChamp = pickTop();
        midChamp = pickMid();
        botChamp = pickBot();
        supChamp = pickSup();
        junChamp = pickJun();
    }
    
    /**
     * Prints top champion
     * @return top champion
     */
    public String printTop() {
        return topChamp;
    }
    
    /**
     * Prints mid champion
     * @return mid champion
     */
    public String printMid() {
        return midChamp;
    }
    
    /**
     * Prints bot champion
     * @return bot champion
     */
    public String printBot() {
        return botChamp;
    }
    
    /**
     * Prints support champion
     * @return sup champion
     */
    public String printSup() {
        return supChamp;
    }
    
    /**
     * Prints jungle champion
     * @return jungle champion
     */
    public String printJun() {
        return junChamp;
    }
    /**
     * Combines two string arrays into one string array
     * @param array1 first string array
     * @param array2 second string array
     * @return string array with elements from the two arrays
     */
    public String[] concatenate(String[] array1, String[] array2) {
        
        int size = array1.length + array2.length;
        String[] newArray = new String[size];
        
        int index = 0;
        
        while(index < array1.length) {
            newArray[index] = array1[index];
            index++;
        }
        
        for(int i = 0; i < array2.length; i++) {
            newArray[index+i] = array2[i];
        }
        
        return newArray;
    }
    /**
     * Picks top champion
     */
    public String pickTop() {
        String[] allPossible = concatenate(top, topmid);
        allPossible = concatenate(allPossible, topjun);
        allPossible = concatenate(allPossible, topsup);
        
        Random rand = new Random();
        int upperbound = allPossible.length;
        int chosenChamp = rand.nextInt(upperbound);
        return allPossible[chosenChamp];
    }
    
    
    /**
     * Picks mid champion, random selection if duplicate
     * @return champion selected for mid
     */
    public String pickMid() {
        String[] allPossible = concatenate(mid, topmid);
        allPossible = concatenate(allPossible, midbot);
        allPossible = concatenate(allPossible, midsup);
        
        midChamp = topChamp;
        
        while(midChamp == topChamp) {
            Random rand = new Random();
            int upperbound = allPossible.length;
            int chosenChamp = rand.nextInt(upperbound);
            midChamp = allPossible[chosenChamp];
        }
        
        return midChamp;
    }
    
    /**
     * Finds bottom champion, repeats random selection if duplicate
     * @return chosen bottom champion
     */
    public String pickBot() {
        String[] allPossible = concatenate(bot, midbot);
        allPossible = concatenate(allPossible, botsup);
        
        botChamp = midChamp;
        
        while(midChamp == botChamp) {
            Random rand = new Random();
            int upperbound = allPossible.length;
            int chosenChamp = rand.nextInt(upperbound);
            botChamp = allPossible[chosenChamp];
        }
        
        return botChamp;
    }
    
    /**
     * Finds support champion that does not duplicate previously chosen champs
     * @return chosen support champ
     */
    public String pickSup() {
        String[] allPossible = concatenate(sup, topsup);
        allPossible = concatenate(allPossible, botsup);
        allPossible = concatenate(allPossible, midsup);
        
        supChamp = topChamp;
        
        while(supChamp == topChamp || botChamp == midChamp || botChamp == supChamp) {
            Random rand = new Random();
            int upperbound = allPossible.length;
            int chosenChamp = rand.nextInt(upperbound);
           supChamp = allPossible[chosenChamp];
        }
        
        return supChamp;
    }
    
    public String pickJun() {
        String[] allPossible = concatenate(jungle, topjun);
        
        junChamp = topChamp;
        
        while(junChamp == topChamp) {
            Random rand = new Random();
            int upperbound = allPossible.length;
            int chosenChamp = rand.nextInt(upperbound);
            junChamp = allPossible[chosenChamp];
        }
        
        return junChamp;
    }


    public static void main(String[] args){
        TeamBuilder team = new TeamBuilder();
        System.out.println("League of Legends Team Generator");
        System.out.println("Top: " + team.printTop());
        System.out.println("Middle: " + team.printMid());
        System.out.println("Bottom: " + team.printBot());
        System.out.println("Support: " + team.printSup());
        System.out.println("Jungle: " + team.printJun());
    }
}
